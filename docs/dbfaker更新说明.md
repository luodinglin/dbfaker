##### 11月11日
字段生成语法支持直接在jinja2模板中调用字段生成方法，举例：
```yaml
package: []
env:
  name: '{{ faker.name() }}'
tables:
- table: stu
  comment: null
  columns:
    # 新增字段生成描述，可直接在jinja2模板中调用生成方法，调用方式参考Python语法
    id: '{{ faker.uuid() }}'
# 老的生成方式，通过engine字段描述字段生成方法
#   id:
#     engine: faker.uuid()
```
##### 11月16日
支持在数据库建表语句的comment字段中描述字段生成规则；详见：[使用comment字段来描述生成规则](https://gitee.com/guojongg/dbfaker/blob/develop/docs/使用comment字段来描述生成规则.md)


#### 22/5/30
支持在配置文件中使用yield实现数据生产有上下文状态：
```yaml
env:
  
  order:
    engine: faker.order  # 通过此方法定义返回一个迭代器对象
    rule:
      value: '{{ list(range(10)) }}'  
- table: course
  comment: null
  columns:
    id:
      comment: null
      engine: next(env["order"])  # 使用next方法调用上述定义的迭代器对象
  max_number: 10 # 指定生成数据量，不能超过迭代器生成数量
  
# 生成数据
dbfaker test.yaml -p

INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (0,'语文','四年级','彭平');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (1,'数学','三年级','范兰英');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (2,'化学','一年级','杨楠');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (3,'地理','六年级','赖玉华');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (4,'化学','二年级','沈桂香');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (5,'英语','四年级','汤彬');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (6,'生物','五年级','李建');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (7,'地理','一年级','孙桂珍');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (8,'语文','四年级','聂秀英');
INSERT INTO `course` (`id`,`name`,`grade`,`teacher`) VALUES (9,'化学','五年级','任凯');
```